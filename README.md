# Osmerix
Rust library for reading and writing [OSM XML](https://wiki.openstreetmap.org/wiki/OSM_XML). Does not support JOSM-style XML.