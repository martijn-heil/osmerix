use std::io::Write;

use xml::escape::escape_str_attribute as esc;
use xml::writer::EventWriter as XmlEventWriter;
use xml::name::Name;
use xml::common::XmlVersion;
use xml::attribute::Attribute;
use osm_primitives::*;

use crate::DocumentMetadata;

pub struct Writer<W: Write>(XmlEventWriter<W>);
const OSM_NAMESPACE = Namespace::empty();

fn meta2attrs(meta: &ElementMetadata) -> [Attribute, 7] {
  return [
    Attribute { name: "id".into(), value: meta.id },
    Attribute { name: "lat".into(), value: meta.lat },
    Attribute { name: "lon".into(), value: meta.lon },
    Attribute { name: "timestamp".into(), value: meta.timestamp },
    Attribute { name: "changeset".into(), value: meta.changeset },
    Attribute { name: "version".into(), value: meta.version },
    Attribute { name: "uid".into(), value: meta.uid },
  ];
}

impl Writer<W: Write> {
  pub fn new<W: Write>(plain_writer: W, metadata: &DocumentMetadata) -> Result<Self, ()> {
    let mut xml_writer = XmlEventWriter::new(plain_writer);

    xml_writer.write(StartDocument {
      version: XmlVersion::Version10,
      encoding: Some("UTF-8"),
      standalone: None,
    })?;


    let mut attrs1 = Vec::new();

    if let metadata.generator = Some(generator) {
      attrs1.push(Attribute {name: "generator".into(), value: generator});
    }

    if let metadata.version = Some(version) {
      attrs1.push(Attribute { name: "version".into(), value: version);
    }

    xml_writer.write(StartElement {
      name: "osm".into(),
      namespace: Cow::from(&OSM_NAMESPACE),
      attributes: Cow::from(attrs1)
    })?;
    xml_writer.write(EndElement { name: None });
    
    return Ok(Writer(xml_writer));
  }
  
  pub fn write(&mut self, what: &Element) -> std::io::Result<()> {
    match what {
      Node(node) => {
        let attrs = meta2attrs(node.metadata);

        self.0.write(StartElement {
          name: "node".into(),
          namespace: Cow::from(&OSM_NAMESPACE),
          attributes: Cow::from(&attrs)
        })?;
        self.output_tags(node.tags)?;
        self.0.write(EndElement { name: "node".into() })?;
      }

      Way(way) => {
        let attrs = meta2attrs(way.metadata);
        self.0.write(StartElement {
          name: "way".into(),
          namespace: Cow::from(&OSM_NAMESPACE),
          attributes: Cow::from(&attrs)
        })?;
        for node in way.nodes {
          self.0.write(StartElement {
            name: "nd".into(),
            namespace: Cow::from(&OSM_NAMESPACE),
            attributes: Cow::from(&[Attribute { name: "ref".into(), value: node.id.to_string().as_str()}])
          })?;
          self.0.write(EndElement { name: "nd".into() })?;
        }
        self.output_tags(way.tags)?;
        self.0.write(EndElement { name: "way".into() })?;
      }
    }
  }

  fn output_tags<I: IntoIterator<&Tag>>(&mut self, tags: I) -> Result<()> {
    for tag in node.tags {
      self.0.write(StartElement {
        name: "tag".into(),
        namespace: Cow::from(&OSM_NAMESPACE),
        attributes: Cow::from([Attribute { name: "k".into(), value: tag.key }, Attribute { name: "v".into(), value: tag.value }])
      })?;
      self.0.write(EndElement { name: "tag".into() })?;
    }
  }
  
  pub fn finish(&mut self) -> std::io::Result<()> {
    self.0.write(EndElement { name: "osm".into() })
  }
}
